--[[
	eon | Eon Install System
	    | Pantheon Project (galaxiorg/pantheon)

	Filename
	  main.lua
	Author
	  daelvn (https://gitlab.com/daelvn)
	Version
		eon-i1
	License
		MIT License (LICENSE.md)
	Documentation
	  manual* eon:about
		* Pantheon/manual : Manual viewer
	Description
		Eon is an install system that uses TOML files for declaring install tasks.
		It features a few yet powerful set of options.
	Dependencies
		Pantheon/libutil? *
		Pantheon/libhttp *
		Pantheon/libconf *
		Pantheon/libwww *
]]--

-- Error
error = printError or error
-- Include (Try to use Pantheon's, else make a replacement)
include = include or function(library)
	local libraryPath = library:gsub(".", "/")
	local prefixes = {
		"/rom/apis/",
		"/rom/apis/command/",
		"/rom/apis/turtle/",
		"/boot/lib/",
		"/lib/",
		""
	}
	local export
	for i, prefix in ipairs(prefixes) do
		if fs.exists(prefix..libraryPath) then
			export = dofile(prefix..libraryPath)
		elseif fs.exists(prefix..libraryPath..".lua") then
			export = dofile(prefix..libraryPath..".lua")
		elseif fs.exists(prefix..libraryPath.."/main.lua") then
			export = dofile(prefix..libraryPath.."/main.lua")
		end
	end
	return export
end

-- Import requirements
local libhttp = include "libhttp"
local toml    = include "libwww.toml"
local flags, getflags, iargs = {include "libutil.pantheon.getflags"}
local libconf = include "libconf"


-- Collect the arguments
local argl   = {...}
local flagstr  = "I(1)$ C(2) s(1) d(0)"
local flagl  = flags(flagstr)
local paired = getflags(argl, flagl)
if not paired then
	error("eon: Could not fetch arguments.")
end
